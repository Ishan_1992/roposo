package com.roposo.roposo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.Vector;

public class Homesceen extends AppCompatActivity {

    Vector<Story_card_Model> mModelVector;
    Context mContext;
    MainApplication mainApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homesceen_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mContext = this;
        mainApplication = (MainApplication) getApplicationContext();

        //Declaring and initialising the Recycler View
        RecyclerView lRecyclerview = (RecyclerView) findViewById(R.id.rv);
        lRecyclerview.setHasFixedSize(true);

        LinearLayoutManager lLinearLayoutManager = new LinearLayoutManager(this);
        lRecyclerview.setLayoutManager(lLinearLayoutManager);

        mModelVector = new Vector<>();

        mModelVector = mainApplication.laodJSON();

        Homescreen_Adapter rvAdapter = new Homescreen_Adapter(mModelVector,this);
        lRecyclerview.setAdapter(rvAdapter);
    }

}
