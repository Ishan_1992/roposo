package com.roposo.roposo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.Vector;

/**
 * Created by Ishan on 21/03/16.
 */
public class Storyscreen extends AppCompatActivity {
    RecyclerView mRecyclerView;
    Vector<Story_Model> mModel;
    MainApplication mainApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storyscreen_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.storyscree_recycler);

        mainApplication = (MainApplication) getApplicationContext();

        mModel = new Vector<>();

        Bundle b = getIntent().getExtras();
        String id = "";
        if(b != null){
             id = b.getString("id");
        }

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.canScrollHorizontally();
        mRecyclerView.setLayoutManager(llm);

        mModel = mainApplication.laodJSON(id);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Storyscreen_Adapter ssA = new Storyscreen_Adapter(mModel, this);
        mRecyclerView.setAdapter(ssA);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        return super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;

        }
        return  true;

    }
}
