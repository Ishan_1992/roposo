package com.roposo.roposo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Vector;

/**
 * Created by Ishan on 21/03/16.
 */
public class Storyscreen_Adapter extends RecyclerView.Adapter<Storyscreen_Adapter.StoryScreen_ViewHoler> {
    Vector<Story_Model> mModel;
    Context mContext;

    public Storyscreen_Adapter(Vector<Story_Model> pModel, Context context){
        mModel = pModel;
        mContext = context;
    }

    @Override
    public StoryScreen_ViewHoler onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_stroyscreen_list, viewGroup, false);
        StoryScreen_ViewHoler storyScreen_ViewHoler = new StoryScreen_ViewHoler(v,mContext,mModel);
        return storyScreen_ViewHoler;
    }

    @Override
    public void onBindViewHolder(StoryScreen_ViewHoler holder, int position) {
        holder.Title.setText(mModel.get(position).getTitle());
        holder.Like.setText("Like: " + mModel.get(position).getLike_count());
        holder.Comment.setText("Comment: " + mModel.get(position).getComment_count());
        holder.Description.setText(mModel.get(position).getDescription());
        Picasso.with(mContext).load(mModel.get(position).getSi()).placeholder(R.drawable.ic_accessibility_24dp).resize(200,250).into(holder.Image);
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    public static class StoryScreen_ViewHoler extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView Title,Like,Comment,Description;
        ImageView Image;
        RelativeLayout mRealtiveLayout;
        Context mContext;
        String id;
        Vector<Story_Model> mModel;


        public StoryScreen_ViewHoler(View itemview,Context c,Vector<Story_Model> pModel){
            super(itemview);
            Title = (TextView) itemview.findViewById(R.id.storyscreen_title);
            Like = (TextView) itemview.findViewById(R.id.storyscreen_like);
            Comment = (TextView) itemview.findViewById(R.id.storyscreen_comment);
            Description = (TextView) itemview.findViewById(R.id.storyscreen_description);
            Image = (ImageView) itemview.findViewById(R.id.storyscreen_image);
            mRealtiveLayout = (RelativeLayout) itemView.findViewById(R.id.storyscreen_master_container);
            mModel = pModel;

            mContext = c;

            mRealtiveLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent details_intent = new Intent(mContext,DetailScreen.class);
            details_intent.putExtra("id", mModel.get(getAdapterPosition()).getId());
            mContext.startActivity(details_intent);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
