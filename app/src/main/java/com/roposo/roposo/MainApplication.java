package com.roposo.roposo;

import android.app.Application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

/**
 * Created by Ishan on 22/03/16.
 */
public class MainApplication extends Application {
    Vector<Story_Model> mModelVector;
    /**
     * Method to load the JSON file from Assests
     *
     * @return
     */
    public Vector<Story_card_Model> laodJSON() {
        Vector<Story_card_Model> pModelVector = new Vector<>();
        try {
            JSONArray jsonArray = new JSONArray(readJson());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);
                Story_card_Model pModel = new Story_card_Model();
                pModel.setUsername(jObj.getString("username"));
                pModel.setHandle(jObj.getString("handle"));
                pModel.setImage(jObj.getString("image"));
                pModel.setAbout(jObj.getString("about"));
                pModel.setId(jObj.getString("id"));
                pModel.setFollowers(jObj.getInt("followers"));
                pModel.setFollowing(jObj.getInt("following"));
                pModel.setUrl(jObj.getString("url"));
                pModelVector.add(pModel);
            }
        } catch (JSONException e) {

        }

        return pModelVector;
    }

    /**
     * Method to load json according to specific ID
     *
     * @param id
     * @return
     */
    public Vector<Story_Model> laodJSON(String id) {
        mModelVector = new Vector<>();
        try {
            JSONArray jsonArray = new JSONArray(readJson());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);
                String db = jObj.optString("db","");
                if (db.equals(id)) {
                    Story_Model pModel = new Story_Model();
                    pModel.setTitle(jObj.getString("title"));
                    pModel.setLike_count(jObj.getInt("likes_count"));
                    pModel.setDescription(jObj.getString("description"));
                    pModel.setComment_count(jObj.getInt("comment_count"));
                    pModel.setSi(jObj.getString("si"));
                    pModel.setId(jObj.getString("id"));
                    pModel.setVerb(jObj.getString("verb"));
                    pModel.setDb(jObj.getString("db"));
                    pModel.setUrl(jObj.getString("url"));
                    pModel.setType(jObj.getString("type"));
                    pModel.setLike_flag(jObj.getBoolean("like_flag"));
                    mModelVector.add(pModel);
                }
            }
        } catch (JSONException e) {

        }

        return mModelVector;
    }

    /**
     * Method to change the Like values.
     * @param id
     */
    public void changeinValue(String id){
        Story_Model pStoryModel;
        for(int i = 0; i< mModelVector.size(); i++){
            pStoryModel = mModelVector.get(i);
            if(pStoryModel.getId().equals(id)){
                pStoryModel.setLike_count(pStoryModel.getLike_count() + 1);
            }
        }

    }

    /**
     * Method used for laoding JSON file for a particular Article
     * @param id
     * @return
     */
    public Story_Model laodJSONwithID(String id) {
        Story_Model pStoryModel = new Story_Model();
        try {
            JSONArray jsonArray = new JSONArray(readJson());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);
                if (jObj.optString("id", "").equals(id)) {
                    pStoryModel.setTitle(jObj.getString("title"));
                    pStoryModel.setLike_count(jObj.getInt("likes_count"));
                    pStoryModel.setDescription(jObj.getString("description"));
                    pStoryModel.setComment_count(jObj.getInt("comment_count"));
                    pStoryModel.setSi(jObj.getString("si"));
                    pStoryModel.setId(jObj.getString("id"));
                    pStoryModel.setVerb(jObj.getString("verb"));
                    pStoryModel.setDb(jObj.getString("db"));
                    pStoryModel.setUrl(jObj.getString("url"));
                    pStoryModel.setType(jObj.getString("type"));
                    pStoryModel.setLike_flag(jObj.getBoolean("like_flag"));
                    break;
                }
            }
        } catch (JSONException e) {

        }

        return pStoryModel;
    }

    /**
     * Method to read the JSON file from Assests project
     * @return
     */
    public String readJson(){
        String json = null;
        try {
            InputStream is = this.getAssets().open("Roposo_main.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;

    }

}
