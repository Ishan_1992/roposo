package com.roposo.roposo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

/**
 * Created by Ishan on 22/03/16.
 */
public class DetailScreen extends AppCompatActivity implements View.OnClickListener{
    TextView mTitle, mLike, mComment, mDescription, mLink;
    ImageView mImage;
    Story_Model mModel;
    String mId;
    MainApplication mainApplication;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_detailscreen_layout);
        mTitle = (TextView) findViewById(R.id.detailslayout_header);
        mLike = (TextView) findViewById(R.id.detailslayout_like);
        mComment = (TextView) findViewById(R.id.detailslayout_comment);
        mDescription = (TextView) findViewById(R.id.detailslayout_description);
        mLink = (TextView) findViewById(R.id.detailslayout_like);
        mImage = (ImageView) findViewById(R.id.detailslayout_image);

        mLike.setOnClickListener(this);
        mContext = this;


        mainApplication = (MainApplication) getApplicationContext();

        Bundle b = getIntent().getExtras();
        String mId = "";
        try {
            if (b != null) {
                mId = b.getString("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mModel = mainApplication.laodJSONwithID(mId);
        setValues();
    }

    public void setValues() {
        mTitle.setText(mModel.getTitle());
        mLike.setText("Like: " + mModel.getLike_count());
        mComment.setText("Comment: " + mModel.getComment_count());
        mDescription.setText(mModel.getDescription());
        mId = mModel.getId();
        Picasso.with(this).load(mModel.getSi()).into(mImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.detailslayout_like:
                Toast.makeText(mContext,"Like",Toast.LENGTH_SHORT).show();
                mainApplication.changeinValue(mId);
                mModel.setLike_count(mModel.getLike_count() + 1);
                mLike.setText("Like: " + mModel.getLike_count());
                default:
                    break;

        }
    }
}