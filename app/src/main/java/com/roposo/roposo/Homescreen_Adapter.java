package com.roposo.roposo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Vector;

/**
 * Created by Ishan on 21/03/16.
 */
public class Homescreen_Adapter extends RecyclerView.Adapter<Homescreen_Adapter.storyViewHolder>{
    Vector<Story_card_Model> mStoryModel;
    Context mContext;

    public Homescreen_Adapter(Vector<Story_card_Model> pStoryModel, Context context){
        mStoryModel = pStoryModel;
        mContext = context;
    }

    @Override
    public void onBindViewHolder(storyViewHolder holder, int position) {
        holder.mTitle.setText(mStoryModel.get(position).username);
        holder.mAbout.setText(mStoryModel.get(position).about);
        holder.mHandle.setText(mStoryModel.get(position).handle);
        holder.mFollower.setText("Followers: " + mStoryModel.get(position).followers);
        holder.mFollowing.setText("Following: " + mStoryModel.get(position).following);

        Picasso.with(mContext).load(mStoryModel.get(position).getImage()).placeholder(R.drawable.ic_face_50dp).resize(150, 150).into(holder.mImageView);
    }

    @Override
    public storyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_homescreen_list, viewGroup, false);
        storyViewHolder stryviewholder = new storyViewHolder(v,mContext,mStoryModel);
        return stryviewholder;
    }


    @Override
    public int getItemCount() {
        return mStoryModel.size();
    }

    public static class storyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CardView cv;
        TextView mTitle,mAbout,mHandle,mFollowing,mFollower;
        ImageView mImageView;
        RelativeLayout mContainer;
        Context mContext;
        Vector<Story_card_Model> mStoryModel;

        public storyViewHolder(View itemView, Context c, Vector<Story_card_Model> pStoryModel) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            mContainer = (RelativeLayout) itemView.findViewById(R.id.master_container);
            mTitle = (TextView) itemView.findViewById(R.id.list_story_header);
            mAbout = (TextView) itemView.findViewById(R.id.list_story_about);
            mHandle = (TextView) itemView.findViewById(R.id.list_story_handle);
            mFollowing = (TextView) itemView.findViewById(R.id.list_story_following);
            mFollower = (TextView) itemView.findViewById(R.id.list_story_follower);
            mImageView = (ImageView) itemView.findViewById(R.id.list_image);
            mStoryModel = pStoryModel;
            mContext = c;

            mContainer.setOnClickListener(this);
        }

        public void startNextActivity(int position){
            Intent story = new Intent(mContext,Storyscreen.class);
            story.putExtra("id",mStoryModel.get(getAdapterPosition()).getId());
            mContext.startActivity(story);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.master_container:
                    startNextActivity(getAdapterPosition());
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
